import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const DetailsScreen = () => {
  return (
    <View style={styles.container}>
      <Text>This is the details screen!</Text>
    </View>
  );
}

DetailsScreen.navigationOptions = {
  title: 'Details'
};

export default DetailsScreen;