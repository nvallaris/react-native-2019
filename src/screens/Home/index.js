import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const HomeScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text>This is the home screen!</Text>
      <Button
          title="Go to details screen"
          onPress={() => navigation.navigate('DetailsScreen')}
        />
    </View>
  );
}

HomeScreen.navigationOptions = {
  title: 'Home'
};

export default HomeScreen;