import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from './src/screens/Home';
import DetailsScreen from './src/screens/Details';

const AppNavigator = createStackNavigator(
  {
    HomeScreen,
    DetailsScreen
  },
  {
    initialRouteName: 'HomeScreen',
    // headerMode: 'none',
  }
);

const ContainerNavigator = createAppContainer(AppNavigator);


export default function App() {
  return <ContainerNavigator/>;
}
